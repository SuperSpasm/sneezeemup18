﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfectedCounterUI : MonoBehaviour {
    [SerializeField] private GameManager manager;
    int Count;
    public Text text;
    private void Start()
    {
        Count = manager.InfectedCount;
        text.text = manager.InfectedCount.ToString();
    }
    // Update is called once per frame
    void Update () {
		if (Count != manager.InfectedCount)
        {
            Count = manager.InfectedCount;
            text.text = manager.InfectedCount.ToString();
        }
	}
}
