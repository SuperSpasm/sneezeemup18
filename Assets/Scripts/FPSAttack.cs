﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSAttack : MonoBehaviour
{
    public string attackName;
    public KeyCode Trigger;
    public float Cooldown;
    public float FOVDistance;
    public float FOVWidth;
    public float FOVFrequency;
    public float PanicDistance;
    public float PanicWidth;
    public float PanicFrequency;
    public AudioSource SoundPlayer;
    public AudioClip[] Sounds;
    public bool WasTriggerd;

    private Quaternion _fovStartingAngle;
    private Quaternion _fovStepAngle;
    private Quaternion _panicStartingAngle;
    private Quaternion _panicStepAngle;
    private bool _canAttack;
    private float _currentCooldown;
    [SerializeField]
    private Image _gauge;

    [SerializeField]
    private ParticleSystem[] _particles;

    // Use this for initialization
    void Start ()
    {
        _fovStartingAngle = Quaternion.AngleAxis(-(FOVWidth / 2), Vector3.up);
        _fovStepAngle = Quaternion.AngleAxis(FOVFrequency, Vector3.up);
        _panicStartingAngle = Quaternion.AngleAxis(-(PanicWidth / 2), Vector3.up);
        _panicStepAngle = Quaternion.AngleAxis(PanicFrequency, Vector3.up);
        _currentCooldown = Cooldown;
    }
	
	// Update is called once per frame
	void Update ()
    {
        // handle cooldown
        if (!_canAttack)
        {
            _currentCooldown -= Time.deltaTime;
            _gauge.fillAmount = _currentCooldown / Cooldown;
            if (_currentCooldown <= Cooldown - 0.2f)
            {
                WasTriggerd = false;

            }
            if(_currentCooldown <= 0)
            {
                _canAttack = true;
                _currentCooldown = Cooldown;
                _gauge.fillAmount = 0;
            }
        }

		if(Input.GetKeyUp(Trigger) && _canAttack)
        {
            _canAttack = false;
            WasTriggerd = true;

            PlaySound();
            PlayParticles();

            List<MobController> detectedMobsForAttackList;
            if (DetectMobsForAttack(out detectedMobsForAttackList))
            {
                //Debug.Log("Mobs Detected For Attack!!!");
                for (int i = 0; i < detectedMobsForAttackList.Count; i++)
                {
                    if (!detectedMobsForAttackList[i].IsInfacted)
                    {
                        detectedMobsForAttackList[i].Infect();
                    }
                }
            }

            List<MobController> detectedMobsForPanicList;
            if (DetectMobsForPanic(out detectedMobsForPanicList))
            {
                //Debug.Log("Mobs Detected For Panic!!!");
                for (int i = 0; i < detectedMobsForPanicList.Count; i++)
                {
                    if (!detectedMobsForPanicList[i].IsInfacted)
                    {
                        detectedMobsForPanicList[i].TriggerPanic();
                    }
                }
            }
        }
	}

    private void PlaySound()
    {
        if (Sounds.Length < 1) return;
        var rnd = Random.Range(0, Sounds.Length);
        SoundPlayer.clip = Sounds[rnd];
        SoundPlayer.Play();
    }

    private void PlayParticles()
    {
        var paritcleSystem = _particles[Random.Range(0, _particles.Length)];
        paritcleSystem.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        paritcleSystem.Play();
    }

    private bool DetectMobsForAttack(out List<MobController> detectedMobsList)
    {
        detectedMobsList = new List<MobController>();
        RaycastHit hit;
        var angle = transform.rotation * _fovStartingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position;

        for (var i = 0; i < FOVWidth / FOVFrequency; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, FOVDistance))
            {
                var enemy = hit.collider.GetComponentInParent<MobController>();
                if(enemy != null)
                {
                    detectedMobsList.Add(enemy);
                }
            }
            direction = _fovStepAngle * direction;
        }

        if(detectedMobsList.Count > 0)
        {
            return true;
        }

        return false;
    }

    private bool DetectMobsForPanic(out List<MobController> detectedMobsList)
    {
        detectedMobsList = new List<MobController>();
        RaycastHit hit;
        var angle = transform.rotation * _panicStartingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position;
        var layerMask = LayerMask.GetMask("Enemy");

        for (var i = 0; i < PanicWidth / PanicFrequency; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, PanicDistance, layerMask))
            {
                var enemy = hit.collider.GetComponentInParent<MobController>();
                if (enemy != null)
                {
                    detectedMobsList.Add(enemy);
                }
            }
            direction = _panicStepAngle * direction;
        }

        if (detectedMobsList.Count > 0)
        {
            return true;
        }

        return false;
    }

    private void OnDrawGizmosSelected()
    {
        _fovStartingAngle = Quaternion.AngleAxis(-(FOVWidth/2), Vector3.up);
        _fovStepAngle = Quaternion.AngleAxis(FOVFrequency, Vector3.up);
        Gizmos.color = Color.red;
        var angle = transform.rotation * _fovStartingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position;
        for (var i = 0; i < FOVWidth/ FOVFrequency; i++)
        {
            Gizmos.DrawRay(transform.position, direction * FOVDistance);
            direction = _fovStepAngle * direction;
        }

        _panicStartingAngle = Quaternion.AngleAxis(-(PanicWidth / 2), Vector3.up);
        _panicStepAngle = Quaternion.AngleAxis(PanicFrequency, Vector3.up);
        Gizmos.color = Color.blue;
        angle = transform.rotation * _panicStartingAngle;
        direction = angle * Vector3.forward;
        pos = transform.position;
        for (var i = 0; i < PanicWidth / PanicFrequency; i++)
        {
            Gizmos.DrawRay(transform.position, direction * PanicDistance);
            direction = _panicStepAngle * direction;
        }
    }
}
