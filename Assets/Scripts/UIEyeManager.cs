﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEyeManager : MonoBehaviour
{
    public Image Eye;
    public Image Warning;

    private static UIEyeManager _instance;
    public static UIEyeManager Instance { get { return _instance; } }

    private float _currentTimeEye;
    private float _currentTimeWarning;
    private float _timeForOff = 0.5f;
    private float _timeForOffForWarning = 5f;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Update()
    {
        _currentTimeEye -= Time.deltaTime;
        _currentTimeWarning -= Time.deltaTime;

        if(_currentTimeEye < 0)
        {
            Eye.gameObject.SetActive(false);
        }
        if (_currentTimeWarning < 0)
        {
            Warning.gameObject.SetActive(false);
        }
        Warning.fillAmount = _currentTimeWarning / _timeForOffForWarning;
    }

    public void ChangeEye()
    {
        Eye.gameObject.SetActive(true);
        _currentTimeEye = _timeForOff;
    }

    public void ChangeWarning()
    {
        Warning.gameObject.SetActive(true);
        Warning.fillAmount = 1;
        _currentTimeWarning = _timeForOffForWarning;
    }
}
