﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PASpeaker : MonoBehaviour {
    public AudioSource source;

    private void Awake()
    {
        GameAudioController.AnnouncementEvent += PlaySound;
    }

    private void OnDestroy()
    {
        GameAudioController.AnnouncementEvent -= PlaySound;
    }

    public void PlaySound(AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }
}
