﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseActorController : MonoBehaviour
{
    public bool IsInfacted;
    public bool IsPaniced;
    public NavMeshAgent Agent;
    public float DieTime;
    public float IdleTime;
    public float RunSpeedBonus;

    protected bool _isInitialized;
    protected bool _hasCustomTarget;
    protected IGameManager _manager;
    protected Vector3 _lastPosition;
    protected float _currentIdleTime;

    // Use this for initialization
    virtual public void Start ()
    {
        _currentIdleTime = IdleTime;
    }

    // Update is called once per frame
    virtual public void Update ()
    {
        if (!_isInitialized) return;
        if (Vector3.Distance(transform.position, _lastPosition) < 0.5f)
        {
            _currentIdleTime -= Time.deltaTime;
            if (_currentIdleTime <= 0)
            {
                _currentIdleTime = IdleTime;
                GetNextPos();
            }
        }
        else
        {
            _lastPosition = transform.position;
            _currentIdleTime = IdleTime;
        }
        if (Agent.remainingDistance < Agent.stoppingDistance && !_hasCustomTarget)
        {
            GetNextPos();
        }
    }

    virtual public void Init(IGameManager manager)
    {
        _manager = manager;

        Agent.speed = Random.Range(0.3f, 2f);
        GetNextPos();
        _isInitialized = true;
    }

    virtual public void GetNextPos()
    {
        var rndX = Random.Range(_manager.xBounds.x, _manager.xBounds.y);
        var rndZ = Random.Range(_manager.zBounds.x, _manager.zBounds.y);

        Agent.destination = new Vector3(rndX, 0, rndZ);
    }

    virtual public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, Agent.destination);
        Gizmos.DrawSphere(Agent.destination, 0.1f);
    }
}
