﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodQuad : MonoBehaviour {

    float originalYPos;
    Vector3 originalLocalScale;
    float originalWorldScale;

    private void Awake()
    {
        originalYPos = transform.position.y;
        originalLocalScale = transform.localScale;
        originalWorldScale = transform.lossyScale.y;
    }

    public void SetQuadPercent(float percent)
    {
        //Debug.LogFormat("Setting quad percent to: {0}", percent);
        Vector3 newLocalScale = originalLocalScale;
        newLocalScale.y = percent * originalLocalScale.y;
        transform.localScale = newLocalScale;

        Vector3 newPos = transform.position;
        newPos.y = originalYPos - (0.5f * (1 - percent) * originalWorldScale);
        transform.position = newPos;
    }
}
