﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TestAgent : MonoBehaviour {
    public NavMeshAgent agent;
    public Transform Goal;

    private void Start()
    {
        agent.SetDestination(Goal.position);
        print("Destination set");
        Log();
    }
    void Update()
    {
        Log();
        StartCoroutine(StartWalking(1, Goal.position));

    }

    private IEnumerator StartWalking(int framesToWait, Vector3 Destination)
    {
        agent.SetDestination(Destination);
        for (int i = 0; i < framesToWait; i++)
        {
            yield return null;
        }

        if (agent.pathStatus != NavMeshPathStatus.PathComplete)
        {
            //Debug.LogFormat("Path status = {0}! destroying.", agent.pathStatus);
            Destroy(gameObject);
        }


        
    }

    private void Log()
    {
        //Debug.LogFormat("hasPath: {0}, pathStatus: {1}, pathPending: {2}, pathStale: {3}", agent.hasPath, agent.pathStatus, agent.pathPending, agent.isPathStale);
    }
}
