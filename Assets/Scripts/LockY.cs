﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockY : MonoBehaviour {

    Transform myTranform;
	void Start () {
        myTranform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        var pos = myTranform.position;
        pos.y = 0;
        myTranform.position = pos;
	}
}
