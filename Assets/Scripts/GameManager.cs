﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public enum GameState
{
    Playing = 0,
    Paused = 1,
    GameOver = 2
}

public class GameManager : MonoBehaviour, IGameManager
{
    public GameState gameState { get; private set; }
    [SerializeField] private FirstPersonController Player;

    [SerializeField] private float gameTime;
    private float countdownTimer;
    [SerializeField] private float FlickerStartPercent;
    [SerializeField] private float UltraFlickerStartPercent;

    [Header("UI")]
    [SerializeField] private GameObject GameOverCanvas;
    [SerializeField] private GameObject OverlayCanvas;
    [SerializeField] private BloodQuad bloodQuad;
    [SerializeField] private Text BloodUI;

    [Header("Enemies")]
    public int NumPatients;
    public int NumDoctors;
    public int Enemies { get; set; }
    public NavMeshAgent GhostAgent;
    public Transform TestGoal;          // ghost agent will try to walk towards this to test if position is valid

    [Header("Managers")]
    public GameAudioController gameAudio;
    public GameLightManager gameLight;

    [Space]

    [SerializeField]
    private Vector2 _xBounds;
    public Vector2 xBounds { get { return _xBounds; } }

    public float yValue;
    [SerializeField] private Vector2 _zBounds;
    public Vector2 zBounds { get { return _zBounds; } }

    private int _InfectedCount = 0;
    public int InfectedCount { get { return _InfectedCount; } }

    private int _EnemyCount = 0;
    public int EnemyCount { get { return _EnemyCount; } }



    public LayerMask GeneratableAreas;          // where enemies can be generated on
    public Transform PatientParent;               // Both doctors and patients will be under this in scene
    public Transform DoctorParent;
    public GameObject PatientPrefab;
    public GameObject DoctorPrefab;

    #region Tests
    public void TestCreatePatients(int amount)
    {
        StartCoroutine(GenerateEnemies<WaitForEndOfFrame>(amount, PatientPrefab, PatientParent));
    }
    public void TestCreateDoctors(int amount)
    {
        StartCoroutine(GenerateEnemies<WaitForEndOfFrame>(amount, DoctorPrefab, DoctorParent));
    }
    #endregion

    void Start()
    {
        StartGame();
    }
    void Update()
    {
        countdownTimer -= Time.deltaTime;

        //Debug.LogFormat("GameTime Percent remain: {0}, lightState: {1}", countdownTimer / gameTime, gameLight.lightState);
        float ultraRemainPercent = (1 - UltraFlickerStartPercent);
        float regRemainPercent = (1 - FlickerStartPercent);

        bloodQuad.SetQuadPercent(countdownTimer / gameTime);

        BloodUI.text = string.Format("{0:n0}%",(countdownTimer / gameTime) * 100);

        if (countdownTimer <= 0)
        {
            OnGameOver();
        }
        else if (gameLight.lightState != GameLightManager.LightState.UltraFlicker 
            && countdownTimer <= ultraRemainPercent * gameTime)
        {
            gameLight.StartUltraFlicker();
        }
        else if (gameLight.lightState == GameLightManager.LightState.AlwaysOn 
            && countdownTimer <= regRemainPercent * gameTime)
        {
            gameLight.StartRegFlicker();
        }
    }

    public void OnGameOver()
    {
        gameState = GameState.GameOver;
        GameOverCanvas.SetActive(true);
        Player.enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
    }

    //void OnGUI()
    //{
    //    GUILayout.Label(string.Format("Time left: {0}", countdownTimer));
    //    GUILayout.Space(20f);
    //    GUILayout.Label(string.Format("Enemies: {0}", EnemyCount));
    //    GUILayout.Label(string.Format("Infected: {0}", InfectedCount));
    //}

    private void StartGame()
    {
        gameState = GameState.Playing;
        Time.timeScale = 1;
        countdownTimer = gameTime;

        GameOverCanvas.SetActive(false);

        if (NumPatients > 0)
        {
            //Debug.LogFormat("About to generate {0} patients", NumPatients);
            StartCoroutine(GenerateEnemies<WaitForEndOfFrame>(NumPatients, PatientPrefab, PatientParent));
        }

        if (NumDoctors > 0)
        {
            //Debug.LogFormat("About to generate {0} doctors", NumDoctors);
            StartCoroutine(GenerateEnemies<WaitForEndOfFrame>(NumDoctors, DoctorPrefab, DoctorParent));
        }

        gameAudio.Ambience_Start();
        gameAudio.BGM_Start();
    }

    void OnDrawGizmos()
    {
        var oldColor = Gizmos.color;
        Gizmos.color = Color.blue;

        #region draw map borders
        Vector3 cubeSize = 1f * Vector3.one;

        Gizmos.DrawCube(new Vector3(xBounds[0], yValue, zBounds[0]), cubeSize);
        Gizmos.DrawCube(new Vector3(xBounds[0], yValue, zBounds[1]), cubeSize);
        Gizmos.DrawCube(new Vector3(xBounds[1], yValue, zBounds[0]), cubeSize);
        Gizmos.DrawCube(new Vector3(xBounds[1], yValue, zBounds[1]), cubeSize);
        #endregion


        Gizmos.color = oldColor;
    }


    public void TogglePause()
    {
        switch (gameState)
        {
            case GameState.Playing:
                PauseGame();
                break;
            case GameState.Paused:
                ResumeGame();
                break;
            case GameState.GameOver:
            default:
                break;
        }
    }
    public void PauseGame()
    {
        gameState = GameState.Paused;
        Player.enabled = false;
        Time.timeScale = 0;
    }
    public void ResumeGame()
    {
        gameState = GameState.Playing;
        Player.enabled = true;
        Time.timeScale = 1;
    }

    #region Enemy Creation
    public IEnumerator GenerateEnemies<Instruction>(int amount, GameObject Prefab, Transform parent)where Instruction : YieldInstruction, new()
    {
        Vector3 randomPos;
        for (int i = 0; i < amount; i++)
        {
            int tryNum = 0;
            while (true)
            {
                // if failed to find a valid position, stop execution
                if (++tryNum > 1000)
                {
                    Debug.LogWarning("Failed to generate random pos for enemy!");
                    yield break;
                }

                // Choose random location
                float x = UnityEngine.Random.Range(xBounds.x, xBounds.y);
                float z = UnityEngine.Random.Range(zBounds.x, zBounds.y);
                
                randomPos = new Vector3(x, yValue, z);

                #region Validate with raycast
                Vector3 rayOrigin = randomPos + Vector3.up;
                Vector3 rayDir = randomPos - rayOrigin;

                bool pointIsValid = Physics.Raycast(rayOrigin, rayDir, maxDistance: 5f, layerMask: GeneratableAreas);
                if (!pointIsValid)
                {
                    continue;
                }
                #endregion

                #region Validate with ghost agent
                //Debug.LogFormat("testing position with ghost agent. position: {0}, randPos: {1}", GhostAgent.transform.position, randomPos);
                GhostAgent.enabled = false;
                GhostAgent.transform.position = randomPos;
                GhostAgent.enabled = true;
                GhostAgent.destination = TestGoal.position;
                yield return null;
                //Debug.LogFormat("moved ghost agent. position: {0}, randPos: {1}", GhostAgent.transform.position, randomPos);
                if (GhostAgent.pathStatus == NavMeshPathStatus.PathComplete)
                {
                    break;
                }
                #endregion

            }

            //Debug.LogFormat("Instantiating enemy at {0}", randomPos);
            InitializeEnemy(Prefab, randomPos, parent);

            yield return new Instruction();
        }
    }

    private void InitializeEnemy(GameObject Prefab, Vector3 randomPos, Transform parent)
    {
        GameObject newEnemy = Instantiate(Prefab);
        newEnemy.name = newEnemy.name + " #" + _EnemyCount;
        newEnemy.transform.position = randomPos;
        newEnemy.transform.parent = parent;
        var newMob = newEnemy.GetComponent<BaseActorController>();
        newMob.Init(this);

        _EnemyCount++;
    }
    #endregion


    #region Callbacks
    public void OnEnemyInfected()
    {
        _InfectedCount++;
    }
    public void OnEnemyDead()
    {
        _EnemyCount--;
        StartCoroutine(GenerateEnemies<WaitForEndOfFrame>(1, PatientPrefab, PatientParent));
    }
    #endregion

    #region Scene loading methods
    public void LoadLevel()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }
    #endregion
}
