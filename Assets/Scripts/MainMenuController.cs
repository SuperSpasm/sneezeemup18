﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public GameObject HelpWindow;

    public void LoadLevel()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowHelp()
    {
        HelpWindow.SetActive(true);
    }

    public void HideHelp()
    {
        HelpWindow.SetActive(false);
    }
}
