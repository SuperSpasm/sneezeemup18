﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameAudioController))]
public class GameAudioEditor : Editor {
    public override void OnInspectorGUI()
    {
        var script = target as GameAudioController;
        if (GUILayout.Button("Test PA"))
        {
            script.PlayRandAnnouncement();
        }
        base.OnInspectorGUI();
    }
}
