﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        var script = target as GameManager;

        if (GUILayout.Button("Create Single Enemy"))
            script.TestCreatePatients(1);
        if (GUILayout.Button("Create 100 Enemies"))
            script.TestCreatePatients(100);

        GUILayout.Space(20f);

        if (GUILayout.Button("Create Doctor"))
            script.TestCreateDoctors(1);
        if (GUILayout.Button("Create 25 Doctors"))
            script.TestCreateDoctors(25);

        base.OnInspectorGUI();
    }
}
