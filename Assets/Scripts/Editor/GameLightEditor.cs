﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameLightManager))]
public class GameLightEditor : Editor{
    public override void OnInspectorGUI()
    {
        var script = target as GameLightManager;
        if (GUILayout.Button("Get All Lights in scene"))
        {
            script.lights = FindObjectsOfType<Light>();
        }

        if (GUILayout.Button("Regular Flicker"))
        {
            script.StartRegFlicker();
        }
        if (GUILayout.Button("Max flicker"))
        {
            script.StartUltraFlicker();
        }
        base.OnInspectorGUI();
    }
}
