﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoctorController : BaseActorController
{
    public float FOVDistance;
    public float FOVWidth;
    public float FOVFrequency;
    public LayerMask LayerToHit;
    public AudioSource chasingSFX;
    [SerializeField] private float KillZoneRange;

    private Quaternion _fovStartingAngle;
    private Quaternion _fovStepAngle;
    [SerializeField]
    private GameObject _chaseIndicator;
    [SerializeField] private Animator _animator;
    private float _chaseStartTime;

    private Transform _playerTransform;

    // Use this for initialization
    override public void Start ()
    {
        base.Start();

        _fovStartingAngle = Quaternion.AngleAxis(-(FOVWidth / 2), Vector3.up);
        _fovStepAngle = Quaternion.AngleAxis(FOVFrequency, Vector3.up);
    }
	
	// Update is called once per frame
	override public void Update ()
    {
        base.Update();

        FPSAttack[] attacks;
        _chaseStartTime -= Time.deltaTime;
        if (DetectAttacks(out attacks))
        {
            UIEyeManager.Instance.ChangeEye();
            //Debug.Log("Player Detected For Attack!!!");
            for (int i = 0; i < attacks.Length; i++)
            {
                if (attacks[i].WasTriggerd)
                {
                    //Debug.Log("Cmhasing Player!!!");
                    chasingSFX.Play();
                    _playerTransform = attacks[i].gameObject.transform;
                    Agent.destination = _playerTransform.position;
                    Agent.speed = Agent.speed + RunSpeedBonus;
                    _hasCustomTarget = true;
                    _chaseStartTime = 5;
                    _animator.speed = 3;
                    UIEyeManager.Instance.ChangeWarning();
                    break;
                }
            }
        }



        _chaseIndicator.SetActive(_hasCustomTarget);
        if (_hasCustomTarget)
        {
            if (_chaseStartTime < 0)
            {
                _hasCustomTarget = false;
                Agent.speed = Agent.speed - RunSpeedBonus;
                _animator.speed = 1;
                chasingSFX.Stop();
            }

            Agent.destination = _playerTransform.position;
            var distance = Vector3.Distance(transform.position, _playerTransform.position);
            //Debug.Log(distance);
            if (distance < KillZoneRange)
            {
                //Debug.Log(gameObject.name + " Killed player");
                _manager.OnGameOver();
                chasingSFX.Stop();
            }
        }
        //if (Agent.remainingDistance < Agent.stoppingDistance && _hasCustomTarget)
        //{
        //    Debug.Log(gameObject.name + " Killed player");
        //    _manager.OnGameOver();
        //}
    }

    override public void OnDrawGizmosSelected()
    {
        _fovStartingAngle = Quaternion.AngleAxis(-(FOVWidth / 2), Vector3.up);
        _fovStepAngle = Quaternion.AngleAxis(FOVFrequency, Vector3.up);
        Gizmos.color = Color.yellow;
        var angle = transform.rotation * _fovStartingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position + new Vector3(0, 0.5f, 0);
        for (var i = 0; i < FOVWidth / FOVFrequency; i++)
        {
            Gizmos.DrawRay(pos, direction * FOVDistance);
            direction = _fovStepAngle * direction;
        }

        // Draw KillZone
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, KillZoneRange);
    }

    private bool DetectAttacks(out FPSAttack[] detectedAttacksList)
    {
        detectedAttacksList = null;
        RaycastHit hit;
        var angle = transform.rotation * _fovStartingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position + new Vector3(0, 0.5f, 0);

        for (var i = 0; i < FOVWidth / FOVFrequency; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, FOVDistance, LayerToHit))
            {
                detectedAttacksList = hit.collider.GetComponents<FPSAttack>();
                if (detectedAttacksList != null && detectedAttacksList.Length > 0)
                {
                    return true;
                }
            }
            direction = _fovStepAngle * direction;
        }

        return false;
    }
}
