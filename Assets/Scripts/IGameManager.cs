﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameManager
{
    // Map bounds
    //float yHeight   { get; }            // the height the agent should be instantiated at
    Vector2 xBounds { get; }            // the Min/Max bounds for the x of the map
    Vector2 zBounds { get; }            // the Min/Max bounds for the z of the map

    int InfectedCount { get; }
    int EnemyCount    { get; }
    GameState gameState { get; }

    IEnumerator GenerateEnemies<Instruction>(int amount, GameObject Prefab, Transform parent) where Instruction : YieldInstruction, new();

    void LoadLevel();
    void LoadMenu();

    void TogglePause();
    void PauseGame();
    void ResumeGame();

    void OnEnemyInfected();
    void OnEnemyDead();
    void OnGameOver();

}
