﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLightManager : MonoBehaviour {
    public Light[] lights;

    [SerializeField] private float regFlickerDelta;
    [SerializeField] private float ultraFlickerDelta;

    public AudioSource flickerSFXSource;

    [Header("Single flicker options")]
    [SerializeField] private float offTime;             // the time the light will be off for
    [SerializeField] private float maxFlickerDelay;
    public enum LightState
    {
        AlwaysOn = 0,
        Flicker = 1,
        UltraFlicker =2
    }
    private Coroutine lightRoutine = null;
    public LightState lightState;

    public void StartUltraFlicker()
    {
        lightState = LightState.UltraFlicker;
        StartCoroutine(StartFlickers(ultraFlickerDelta));
    }
    public void StartRegFlicker()
    {
        lightState = LightState.Flicker;
        StartCoroutine(StartFlickers(regFlickerDelta));
    }

    public IEnumerator StartFlickers(float deltaTime)
    {
        while (true)
        {
            flickerSFXSource.Play();
            Debug.LogFormat("Flickering w/ deltaTime {0}", deltaTime);
            foreach (var light in lights)
            {
                StartCoroutine(Flicker(light));
            }
            yield return new WaitForSeconds(deltaTime);
        }
    }

    public IEnumerator Flicker(Light light)
    {
        float delay = UnityEngine.Random.Range(0, maxFlickerDelay);

        yield return new WaitForSeconds(delay);

        light.enabled = false;

        yield return new WaitForSeconds(offTime);

        light.enabled = true;
    }
}
