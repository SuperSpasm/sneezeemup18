﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameAudioController : MonoBehaviour {

    public const string RegularLoopVolName = "RegLoopVol";
    public const string TransistorLoopVolName = "TranLoopVol";

    [SerializeField] private Vector2 AnnoucementDeltaRange;

    public AudioSource BGMSource;
    public AudioSource AmbienceSource;
    public AudioMixerGroup ReuglarLoopChannel;
    public AudioMixerGroup TransistorLoopChannel;

    [Space]
    public AudioClip[] BGMLoops;
    public AudioClip[] annoucementClips;
    public int lastAnnounceIndex = -1;


    private enum AudioState { Playing, Paused, Stopped }
    private AudioState audioState = AudioState.Stopped;

    public enum LoopType { Regular, Transistor }
    public LoopType startLoopType;
    private LoopType loopType;

    //TODO: finish loop logic (triggers/etc)

    private void Start()
    {
        StartCoroutine(StartAnnouncements());
    }
    private IEnumerator StartAnnouncements()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(AnnoucementDeltaRange[0], AnnoucementDeltaRange[1]));
            PlayRandAnnouncement();
        }
    }
    // publically exposed methods
    public void BGM_Start()
    {
        //print("BGM_Start()");

        loopType = startLoopType;

        audioState = AudioState.Playing;
        SetRandomClip();
        BGMSource.Play();
    }
    public void BGM_PausePlay()
    {
        switch (audioState)
        {
            case AudioState.Playing:
                BGMSource.Pause();
                audioState = AudioState.Paused;
                break;
            case AudioState.Stopped:
                SetRandomClip();
                BGMSource.Play();
                audioState = AudioState.Playing;
                break;
            case AudioState.Paused:
                BGMSource.UnPause();
                audioState = AudioState.Playing;
                break;
            default:
                break;
        }
    }
    public void BGM_Stop()
    {
        BGMSource.Stop();
        audioState = AudioState.Stopped;
    }

    public void Ambience_Start()
    {
        AmbienceSource.Play();
    }
    public void Ambience_Stop()
    {
        AmbienceSource.Stop();
    }

    public delegate void AnnouncementEventHandler(AudioClip clip);
    public static event AnnouncementEventHandler AnnouncementEvent;

    private void Update()
    {
        if (audioState == AudioState.Playing && !BGMSource.isPlaying)
        {
            SetRandomClip();
            BGMSource.Play();
        }
    }

    public void PlayRandAnnouncement()
    {
        int i;
        do
        {
            i = UnityEngine.Random.Range(0, annoucementClips.Length);
        } while (i == lastAnnounceIndex);

        AudioClip clip = annoucementClips[i];
        if (AnnouncementEvent != null)
        {
            AnnouncementEvent(clip);
        }

        lastAnnounceIndex = i;
    }

    public void TransitionLoop(LoopType newLoop)
    {
        if (newLoop == loopType)
        {
            return;
        }

        switch (newLoop)
        {
            case LoopType.Regular:
                ;
                break;
            case LoopType.Transistor:
                break;
            default:
                break;
        }

    }

    private void SetRandomClip()
    {
        int i = UnityEngine.Random.Range(0, BGMLoops.Length);
        BGMSource.clip = BGMLoops[i];
    }
}
