﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobController : BaseActorController
{
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip[] infectionReactionSounds;
    [SerializeField] private float chanceToSound;

    [SerializeField] private float minTimeBetweenSounds;
    [SerializeField] private Animator _animator;
    private float nextSoundTimer;

    private Transform _doctorTransform;
    // Update is called once per frame
    override public void Update ()
    {
        base.Update();

        if (nextSoundTimer > 0)
        {
            nextSoundTimer -= Time.deltaTime;
        }

        if(IsInfacted)
        {
            DieTime -= Time.deltaTime;
            if(DieTime <= 0)
            {
                _manager.OnEnemyDead();
                Destroy(gameObject);
            }
        }

        if(IsPaniced && _doctorTransform != null)
        {
            Agent.destination = _doctorTransform.position;
        }
        _animator.SetBool("IsWalking", !IsPaniced);
        _animator.SetBool("IsRunning", IsPaniced);
        Agent.speed = IsPaniced ? 2 : 1;
        if (Agent.remainingDistance < Agent.stoppingDistance && _hasCustomTarget)
        {
            _hasCustomTarget = false;
            IsPaniced = false;
            Agent.speed = Agent.speed - RunSpeedBonus;
        }
    }

    public void Infect()
    {
        IsInfacted = true;
        gameObject.layer = LayerMask.NameToLayer("InfectedEnemy");
        if (nextSoundTimer <= 0 && UnityEngine.Random.Range(0f, 1f) <= chanceToSound)
        {
            PlayRndSFX();
        }
        var renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        renderer.material.color = Color.green;
        _manager.OnEnemyInfected();
    }

    public void TriggerPanic()
    {
        IsPaniced = true;
        var docs = GameObject.FindGameObjectsWithTag("Doctor");
        if (docs.Length < 1) return;
        var _closestDistance = float.MaxValue;
        var _closestIndex = -1;
        for (int i = 0; i < docs.Length; i++)
        {
            var distance = Vector3.Distance(transform.position, docs[i].transform.position);
            if(distance < _closestDistance)
            {
                _closestDistance = distance;
                _closestIndex = i;
            }
        }
        _doctorTransform = docs[_closestIndex].transform;
        Agent.destination = _doctorTransform.position;
        _hasCustomTarget = true;
        Agent.speed = Agent.speed + RunSpeedBonus;
    }

    private void PlayRndSFX()
    {
        int i = UnityEngine.Random.Range(0, infectionReactionSounds.Length);
        source.clip = infectionReactionSounds[i];
        source.Play();

        nextSoundTimer = minTimeBetweenSounds;
    }
}
